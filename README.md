# UavTest

#### 介绍
无人机系统测试平台管理系统

#### 软件架构
1. 采用 vue-cli v3 版本作为脚手架
2. 涉及技术栈及框架： vue.js 2.x， Element 2.x， es6， webpack4， axios， lodash


#### 安装教程
0. 请首先确认是否已安装node.js环境，没有请先到node官网下载8.10以上版本并安装
1. 克隆本项目，完成后进入项目根目录，打开命令窗口（cmd，bash等）
2. 安装 vue-cli 3 脚手架，命令为： npm install -g @vue/cli
3. 安装依赖，命令为： npm install
4. 启动服务，命令为： npm run dev
5. 开始开发

#### 使用说明

1. 发布，构建生产环境，命令为： npm run build:prod
2. 预览发布环境效果： npm run preview
3. 预览发布环境效果 + 静态资源分析： npm run preview -- --report
4. 代码格式检查： npm run lint
5. 代码格式检查并自动修复： npm run lint -- --fix
