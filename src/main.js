import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n
import zhLocale from 'element-ui/lib/locale/lang/zh-CN'

import '@/styles/index.scss' // global css

import App from './App'
import store from './store'
import router from './router'
import lodash from 'lodash'

import '@/icons' // icon
import '@/permission' // permission control
import settings from '@/settings'

// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
Vue.use(ElementUI, { zhLocale })
Vue.prototype._ = lodash
Vue.prototype.loadingConfig = {
  lock: true,
  text: '数据加载中',
  spinner: 'el-icon-loading',
  background: 'rgba(0, 0, 0, 0.85)'
}

Vue.config.productionTip = false
Vue.prototype.settings = settings

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
