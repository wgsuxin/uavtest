module.exports = {

  title: '无人系统通用测试平台',

  /**
   * @type {string}
   * @description 接口基础地址
   */
  // apiBaseUrl: 'http://39.96.35.206:8088', //开发环境代理地址
  apiBaseUrl: 'http://webapi01.efly-cetc.com', //开发环境代理地址
  defaultApiBaseUrl: process.env.VUE_APP_UAVAPI_BASE, //统一接口前缀 方便部署后修改

  /**
   * @type {string}
   * @description 网站根目录绝对路径
   */
  webRoot: '/',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  sidebarLogo: false
}
