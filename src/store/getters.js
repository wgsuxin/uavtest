const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
	user_title:state=>state.user.user_title,
	id:state=> state.user.id,
	departmentNameStr:state=>state.user.departmentNameStr,
}
export default getters
