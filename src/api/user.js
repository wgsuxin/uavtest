// import request from '@/utils/request'

// request2 为线上请求封装
import request2 from '@/utils/request-new'

export function login(data) {
  return request2({
    url: '/user/userLogin',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request2({
    url: '/user/profile',
    method: 'post',
    data: { token: token }
  })
}

export function logout() {
  // 采用线上接口
  return request2({
    url: '/user/logout',
    method: 'post'
  })
}
