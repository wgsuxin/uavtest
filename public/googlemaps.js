var myLatlng = new google.maps.LatLng(39.542990, 110.093091);
var wsUri_1 = "ws://39.96.35.206:19504";
// var wsUri = "ws://39.102.157.141:19504"; //新服务器

var mapOptions = {
    zoom: 13,
    center: myLatlng,
    panControl: true,
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    overviewMapControl: true
}

var map;
var opt;
var addMarker1;
var i = 1;
var contentString;
var addPolyline1;
var polylineArray = new Array();
var dragPolygonArray = new Array();
var dragPolygonMarkerArray = new Array();
var polygonArray = new Array();
var markerCenterArray = new Array();
var markerRadiusArray = new Array();
var circleArray = new Array();
var addCircleArray = new Array();
var circle;

var markerArray = new Array();
var postMarkerArray = new Array();

var bounds = {
    16: [
        [52807, 52822],
        [24906, 24920]
    ],
    17: [
        [105615, 105645],
        [49813, 49840]
    ],
    18: [
        [211230, 211290],
        [99626, 99681]
    ],
    19: [
        [422460, 422580],
        [199253, 199363]
    ],
    20: [
        [844920, 845160],
        [398506, 398726]
    ],
    21: [
        [1689840, 1690321],
        [797012, 797452]
    ],
    22: [
        [3379680, 3380643],
        [1594025, 1594906]
    ],
    23: [
        [6759361, 6761286],
        [3188051, 3189811]
    ]
};

var image = 'http://39.96.33.244:8082/gapi3/api-3/images/spotlight-poi.png';

var infoWindow;

function ImageMapType() {}

ImageMapType.prototype.tileSize = new google.maps.Size(256, 256);
ImageMapType.prototype.maxZoom = 23; // 地图显示最大级别
ImageMapType.prototype.minZoom = 1; // 地图显示最小级别
ImageMapType.prototype.name = "本地地图";
ImageMapType.prototype.alt = "显示本地地图数据";
ImageMapType.prototype.getTile = function(coord, zoom, ownerDocument) {
    var img = ownerDocument.createElement("img");
    img.style.width = this.tileSize.width + "px";
    img.style.height = this.tileSize.height + "px";

    //	var strURL = "http://39.96.33.244/images/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    var strURL = "https://qzfeihong.oss-cn-beijing.aliyuncs.com/feihong23/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    //	var strURL = "maptile/" + zoom + "/" + coord.x + "/" + coord.y + ".png";

    //	if (zoom==16){
    //		strURL = "https://qzgoogle.oss-cn-beijing.aliyuncs.com/google23/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    //	}if(zoom==17){
    //		strURL = "https://qzgoogle.oss-cn-beijing.aliyuncs.com/google23/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    //	}if(zoom==18){
    //		strURL = "https://qzgoogle.oss-cn-beijing.aliyuncs.com/google23/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    //	}if(zoom==19){
    //		strURL = "https://qzgoogle.oss-cn-beijing.aliyuncs.com/google23/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    //	}if(zoom==20){
    //		strURL = "http://39.96.35.206/images/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    //	}if(zoom==21){
    //		strURL = "http://39.96.35.206/images/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    //	}

    if (zoom < 16 || zoom > 23 || bounds[zoom][0][0] > coord.x ||
        coord.x > bounds[zoom][0][1] || bounds[zoom][1][0] > coord.y ||
        coord.y > bounds[zoom][1][1]) {
        strURL = "https://qzfeihong.oss-cn-beijing.aliyuncs.com/feihong23/16/52808/24917.png";
        //		strURL = "http://39.96.33.244/images/16/52808/24917.png";
        //		strURL = "maptile/16/52808/24917.png";
    }

    img.src = strURL;
    return img;
};

var ImageMapType = new ImageMapType();

var bounds2 = {
    1: [
        [1, 1],
        [0, 0]
    ],
    2: [
        [3, 3],
        [1, 1]
    ],
    3: [
        [6, 6],
        [3, 3]
    ],
    4: [
        [12, 12],
        [6, 6]
    ],
    5: [
        [25, 25],
        [12, 12]
    ],
    6: [
        [51, 51],
        [24, 24]
    ],
    7: [
        [103, 103],
        [48, 48]
    ],
    8: [
        [206, 206],
        [97, 97]
    ],
    9: [
        [412, 412],
        [194, 194]
    ],
    10: [
        [824, 825],
        [388, 389]
    ],
    11: [
        [1649, 1651],
        [777, 779]
    ],
    12: [
        [3299, 3302],
        [1554, 1559]
    ],
    13: [
        [6598, 6605],
        [3109, 3118]
    ],
    14: [
        [13196, 13210],
        [6219, 6237]
    ],
    15: [
        [26392, 26421],
        [12439, 12474]
    ],
    16: [
        [52785, 52843],
        [24879, 24949]
    ],
    17: [
        [105571, 105687],
        [49758, 49899]
    ],
    18: [
        [211142, 211374],
        [99517, 99799]
    ],
    19: [
        [422285, 422748],
        [199034, 199599]
    ]
};

function LocalMapType() {}

LocalMapType.prototype.tileSize = new google.maps.Size(256, 256);
LocalMapType.prototype.maxZoom = 16;
LocalMapType.prototype.minZoom = 5;
LocalMapType.prototype.name = "本地";
LocalMapType.prototype.alt = "显示本地地图";
LocalMapType.prototype.getTile = function(coord, zoom, ownerDocument) {
    var img = ownerDocument.createElement("img");
    img.style.width = this.tileSize.width + "px";
    img.style.height = this.tileSize.height + "px";

    //	var strURL = "http://39.96.33.244/downloads/" + zoom + "/" + coord.x + "/" + coord.y + ".jpg";
    //	var strURL = "https://qzgoogle.oss-cn-beijing.aliyuncs.com/downloads/" + zoom + "/" + coord.x + "/" + coord.y + ".jpg";
    var strURL = "http://mt1.google.cn/maps/vt?lyrs=y@821&hl=zh-CN&gl=CN&x=" + coord.x + "&y=" + coord.y + "&z=" + zoom;
    // var strURL = "http://www.google.cn/maps/vt?lyrs=s@100&x=" + coord.x + "&y=" + coord.y + "&z=" + zoom;
    //	var strURL = "maptile/download/" + zoom + "/" + coord.x + "/" + coord.y + ".jpg";
    //	if (zoom > 19) {
    ////		strURL = "https://qzgoogle.oss-cn-beijing.aliyuncs.com/google/16/52808/24917.png";
    //		strURL = "http://mt1.google.cn/maps/vt?lyrs=s@821&hl=zh-CN&gl=CN&x=" + coord.x + "&y=" + coord.y + "&z=" + zoom;
    ////		strURL = "http://39.96.33.244/images/16/52808/24917.png";
    ////		strURL = "maptile/16/52808/24917.png";
    //	}
    //	if (zoom > 19 || bounds2[zoom][0][0] > coord.x
    //			|| coord.x > bounds2[zoom][0][1] || bounds2[zoom][1][0] > coord.y
    //			|| coord.y > bounds2[zoom][1][1]) {
    //		strURL = "https://qzgoogle.oss-cn-beijing.aliyuncs.com/google23/16/52808/24917.png";
    ////		strURL = "http://39.96.33.244/images/16/52808/24917.png";
    ////		strURL = "maptile/16/52808/24917.png";
    //	}
    img.src = strURL;
    return img;
};

var localMapType = new LocalMapType();

function LocalMapType1() {}

LocalMapType1.prototype.tileSize = new google.maps.Size(256, 256);
LocalMapType1.prototype.maxZoom = 18;
LocalMapType1.prototype.minZoom = 1;
LocalMapType1.prototype.name = "本地";
LocalMapType1.prototype.alt = "显示本地地图";
LocalMapType1.prototype.getTile = function(coord, zoom, ownerDocument) {
    var img = ownerDocument.createElement("img");
    img.style.width = this.tileSize.width + "px";
    img.style.height = this.tileSize.height + "px";

    //	var strURL = "http://39.96.33.244/downloads/" + zoom + "/" + coord.x + "/" + coord.y + ".jpg";
    //	var strURL = "https://qzgoogle.oss-cn-beijing.aliyuncs.com/downloads/" + zoom + "/" + coord.x + "/" + coord.y + ".jpg";
    //	var strURL = "http://mt1.google.cn/maps/vt?lyrs=s@821&hl=zh-CN&gl=CN&x=" + coord.x + "&y=" + coord.y + "&z=" + zoom;
    // var strURL = "http://mt1.google.cn/maps/vt?lyrs=m@821&hl=zh-CN&gl=CN&x=" + coord.x + "&y=" + coord.y + "&z=" + zoom;
    // var strURL = "http://mt1.google.cn/maps/vt?lyrs=y@821&hl=zh-CN&gl=CN&x=" + coord.x + "&y=" + coord.y + "&z=" + zoom;
    var strURL = "http://www.google.cn/maps/vt?lyrs=s@100&x=" + coord.x + "&y=" + coord.y + "&z=" + zoom;
    //	var strURL = "maptile/download/" + zoom + "/" + coord.x + "/" + coord.y + ".jpg";
    //	if (zoom > 19) {
    ////		strURL = "https://qzgoogle.oss-cn-beijing.aliyuncs.com/google/16/52808/24917.png";
    //		strURL = "http://mt1.google.cn/maps/vt?lyrs=s@821&hl=zh-CN&gl=CN&x=" + coord.x + "&y=" + coord.y + "&z=" + zoom;
    ////		strURL = "http://39.96.33.244/images/16/52808/24917.png";
    ////		strURL = "maptile/16/52808/24917.png";
    //	}
    //	if (zoom > 19 || bounds2[zoom][0][0] > coord.x
    //			|| coord.x > bounds2[zoom][0][1] || bounds2[zoom][1][0] > coord.y
    //			|| coord.y > bounds2[zoom][1][1]) {
    //		strURL = "https://qzgoogle.oss-cn-beijing.aliyuncs.com/google23/16/52808/24917.png";
    ////		strURL = "http://39.96.33.244/images/16/52808/24917.png";
    ////		strURL = "maptile/16/52808/24917.png";
    //	}
    img.src = strURL;
    return img;
};

var localMapType1 = new LocalMapType1();

var bounds1 = {
    16: [
        [54110, 54111],
        [24764, 24765]
    ],
    17: [
        [108220, 108222],
        [49529, 49531]
    ],
    18: [
        [216440, 216444],
        [99059, 99063]
    ],
    19: [
        [432880, 432888],
        [198119, 198127]
    ],
    20: [
        [865761, 865776],
        [396239, 396254]
    ],
    21: [
        [1731523, 1731553],
        [792479, 792509]
    ],
    22: [
        [3463047, 3463107],
        [1584959, 1585018]
    ],
    23: [
        [6926094, 6926214],
        [3169919, 3170036]
    ]
};

function ImageMapType1() {}

ImageMapType1.prototype.tileSize = new google.maps.Size(256, 256);
ImageMapType1.prototype.maxZoom = 23; // 地图显示最大级别
ImageMapType1.prototype.minZoom = 1; // 地图显示最小级别
ImageMapType1.prototype.name = "本地地图";
ImageMapType1.prototype.alt = "显示本地地图数据";
ImageMapType1.prototype.getTile = function(coord, zoom, ownerDocument) {
    var img = ownerDocument.createElement("img");
    img.style.width = this.tileSize.width + "px";
    img.style.height = this.tileSize.height + "px";

    //	var strURL = "http://39.96.33.244/images/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    var strURL = "https://qzfeihong.oss-cn-beijing.aliyuncs.com/changping23/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    //	var strURL = "maptile/" + zoom + "/" + coord.x + "/" + coord.y + ".png";

    //	if (zoom==16){
    //		strURL = "https://qzgoogle.oss-cn-beijing.aliyuncs.com/google23/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    //	}if(zoom==17){
    //		strURL = "https://qzgoogle.oss-cn-beijing.aliyuncs.com/google23/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    //	}if(zoom==18){
    //		strURL = "https://qzgoogle.oss-cn-beijing.aliyuncs.com/google23/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    //	}if(zoom==19){
    //		strURL = "https://qzgoogle.oss-cn-beijing.aliyuncs.com/google23/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    //	}if(zoom==20){
    //		strURL = "http://39.96.35.206/images/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    //	}if(zoom==21){
    //		strURL = "http://39.96.35.206/images/" + zoom + "/" + coord.x + "/" + coord.y + ".png";
    //	}

    if (zoom < 16 || zoom > 23 || bounds1[zoom][0][0] > coord.x ||
        coord.x > bounds1[zoom][0][1] || bounds1[zoom][1][0] > coord.y ||
        coord.y > bounds1[zoom][1][1]) {
        strURL = "https://qzfeihong.oss-cn-beijing.aliyuncs.com/feihong23/16/52808/24917.png";
        //		strURL = "http://39.96.33.244/images/16/52808/24917.png";
        //		strURL = "maptile/16/52808/24917.png";
    }

    img.src = strURL;
    return img;
};

var ImageMapType1 = new ImageMapType1();

function initialize(divName, opt) {
    map = new google.maps.Map(document.getElementById(divName), mapOptions);

    map.setOptions(opt);

    map.mapTypes.set('local', localMapType);
    map.setMapTypeId('local');
    map.overlayMapTypes.setAt("1", ImageMapType);
}

function setMaps() {
    map.overlayMapTypes.setAt("1", ImageMapType);
    map.overlayMapTypes.setAt("2", ImageMapType1);
}

function removeMaps() {
    map.overlayMapTypes.removeAt("1");
    map.overlayMapTypes.removeAt("2");
    removeMaps();
}

function setMarkers(locations) {
    removeAllMarkers();
    if (locations && locations.length > 0) {
        for (var i = 0; i < locations.length; i++) {
            var beach = locations[i];
            var myLatLng1 = new google.maps.LatLng(beach[1], beach[2]);
            var marker = new google.maps.Marker({
                position: myLatLng1,
                map: map,
                title: beach[0],
                icon: image
            });

            markerArray.push(marker);
        }
    }
}

function showAllMarkers() {
    hideAllMarkers();
    if (markerArray && markerArray.length > 0) {
        for (var i = 0; i < markerArray.length; i++) {
            var itemMarker = markerArray[i];
            itemMarker.setMap(map);
        }
    }
}

function removeAllMarkers() {
    if (markerArray && markerArray.length > 0) {
        for (var i = 0; i < markerArray.length; i++) {
            var itemMarker = markerArray[i];
            itemMarker.setMap(null);

        }
    }
    markerArray = new Array();
}

function hideAllMarkers() {
    var markerArray1 = markerArray;
    if (markerArray1 && markerArray1.length > 0) {
        for (var i = 0; i < markerArray1.length; i++) {
            var itemMarker = markerArray1[i];
            itemMarker.setMap(null);

        }
    }
}


function setBounds(mylatLngBounds) {
    google.maps.event.addListener(map, 'dragend', function() {
        if (mylatLngBounds.contains(map.getCenter())) return;

        var c = map.getCenter(),
            x = c.lng(),
            y = c.lat(),
            maxX = mylatLngBounds.getNorthEast().lng(),
            maxY = mylatLngBounds.getNorthEast().lat(),
            minX = mylatLngBounds.getSouthWest().lng(),
            minY = mylatLngBounds.getSouthWest().lat();

        if (x < minX) x = minX;
        if (x > maxX) x = maxX;
        if (y < minY) y = minY;
        if (y > maxY) y = maxY;

        map.setCenter(new google.maps.LatLng(y, x));
    });

    google.maps.event.addListener(map, 'zoom_changed', function() {
        if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
    });
}

function addMarker(location, title, image, contentString) {
    var marker = new google.maps.Marker({
        position: location,
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
        title: title,
        icon: image
    });

    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });


    //    infowindow.open(map, marker);
    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });

    markerArray.push(marker);
    i++;
}

function oneMarker(event) {

    contentString = '<div id="content">' +
        '<div id="siteNotice">' +
        '</div>' +
        '<h1 id="firstHeading" class="firstHeading">marker</h1>' +
        '<div id="bodyContent">' +
        '<p>add marker ' + i + '</p>' +
        '</div>' +
        '</div>';

    addMarker(event.latLng, i, image, contentString);
}

function toggleBounce() {
    if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
    } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
    }
}


function addLatLng(event) {

    var path = poly.getPath();

    path.push(event.latLng);

    //  var marker = new google.maps.Marker({
    //    position: event.latLng,
    //    title: '#' + path.getLength(),
    //    draggable: true,
    //    map: map
    //  });
}

function showArrays(event) {
    var vertices = this.getPath();

    var contentString = '<b>polygon' + m + '</b><br>' +
        'Clicked location: <br>' + event.latLng.lat() + ',' + event.latLng.lng() +
        '<br>';

    for (var i = 0; i < vertices.getLength(); i++) {
        var xy = vertices.getAt(i);
        contentString += '<br>' + 'Coordinate ' + i + ':<br>' + xy.lat() + ',' + xy.lng();
    }

    infoWindow.setContent(contentString);
    infoWindow.setPosition(event.latLng);

    infoWindow.open(map);
}

function getPath() {
    var path = flightPath.getPath();
    var len = path.getLength();
    var coordStr = "";
    for (var i = 0; i < len; i++) {
        coordStr += path.getAt(i).toUrlValue(6) + "<br>";
    }
    document.getElementById('path').innerHTML = coordStr;
}

function calculationDistance(lat1, lon1, lat2, lon2) {
    var R = 6371; //地球半径 km  
    var dLat = (lat2 - lat1) * Math.PI / 180;
    var dLon = (lon2 - lon1) * Math.PI / 180;
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
}

function showMap() {
    setMaps();
}

function hideMap() {
    removeMaps();
}

function showPad() {
    showAllMarkers();
}

function hidePad() {
    hideAllMarkers();
}

function addOneMarker() {
    endMarker();
    endPolyline();
    endPolygon();

    addMarker1 = google.maps.event.addListener(map, 'click', function(event) {

        var contentString = '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading">marker' + i + '</h1>' +
            '<div id="bodyContent">' +
            '<p>lat: ' + event.latLng.lat() + '<br>lng:' + event.latLng.lng() + '</p>' +
            '</div>' +
            '</div>';

        addMarker(event.latLng, 'title' + i, image, contentString);
    });
}

function endMarker() {
    google.maps.event.removeListener(addMarker1);
}

function addPolyline() {
    endPolyline();
    endMarker();
    endPolygon();

    var polyOptions = {
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 3,
        editable: true
    }
    poly = new google.maps.Polyline(polyOptions);
    poly.setMap(map);

    addPolyline1 = google.maps.event.addListener(map, 'click', addLatLng);
    polylineArray.push(poly);
}

function endPolyline() {
    google.maps.event.removeListener(addPolyline1);

    if (polylineArray && polylineArray.length > 0) {
        for (var i = 0; i < polylineArray.length; i++) {
            var item = polylineArray[i];
            item.setEditable(false);
        }
    }
}

function addPolygon() {
    endPolyline();
    endMarker();
    endPolygon();

    var isClosed = false;
    var poly = new google.maps.Polyline({ map: map, path: [], strokeColor: "#FF0000", strokeOpacity: 1.0, strokeWeight: 2 });
    google.maps.event.addListener(map, 'click', function(clickEvent) {
        if (isClosed)
            return;
        var markerIndex = poly.getPath().length;
        var isFirstMarker = markerIndex === 0;
        var marker = new google.maps.Marker({ map: map, position: clickEvent.latLng, draggable: true });
        if (isFirstMarker) {
            google.maps.event.addListener(marker, 'click', function() {
                if (isClosed)
                    return;
                var path = poly.getPath();
                poly.setMap(null);
                poly = new google.maps.Polygon({ map: map, path: path, strokeColor: "#FF0000", strokeOpacity: 0.8, strokeWeight: 2, fillColor: "#FF0000", fillOpacity: 0.35 });
                var m = polygonArray.length + 1;
                poly.addListener('click', function(event) {
                    var vertices = this.getPath();

                    var contentString = '<b>polygon' + m + '</b><br>' +
                        'Clicked location: <br>' + event.latLng.lat() + ',' + event.latLng.lng() +
                        '<br>';

                    for (var i = 0; i < vertices.getLength(); i++) {
                        var xy = vertices.getAt(i);
                        contentString += '<br>' + 'Coordinate ' + i + ':<br>' + xy.lat() + ',' + xy.lng();
                    }

                    infoWindow.setContent(contentString);
                    infoWindow.setPosition(event.latLng);

                    infoWindow.open(map);
                });
                infoWindow = new google.maps.InfoWindow;
                polygonArray.push(poly);

                isClosed = true;
            });
        }
        var dragPolygon = google.maps.event.addListener(marker, 'drag', function(dragEvent) {
            poly.getPath().setAt(markerIndex, dragEvent.latLng);
        });
        dragPolygonArray.push(dragPolygon);
        dragPolygonMarkerArray.push(marker);
        poly.getPath().push(clickEvent.latLng);
    });


}

function endPolygon() {
    if (dragPolygonArray && dragPolygonArray.length > 0) {
        for (var i = 0; i < dragPolygonArray.length; i++) {
            var item = dragPolygonArray[i];
            google.maps.event.removeListener(item);
            var itemMarker = dragPolygonMarkerArray[i];
            itemMarker.setMap(null);
        }
    }
    if (polygonArray && polygonArray.length > 0) {
        for (var i = 0; i < polygonArray.length; i++) {
            var item = polygonArray[i];
            item.setEditable(false);
        }
    }
    dragPolygonArray = new Array();
    dragPolygonMarkerArray = new Array();
}

function hidePolygon() {
    if (polygonArray && polygonArray.length > 0) {
        for (var i = 0; i < polygonArray.length; i++) {
            var item = polygonArray[i];
            item.setMap(null);
        }
    }
}

function showPolygon() {
    if (polygonArray && polygonArray.length > 0) {
        for (var i = 0; i < polygonArray.length; i++) {
            var item = polygonArray[i];
            item.setMap(map);
        }
    }
}

function editPolygon() {
    if (polygonArray && polygonArray.length > 0) {
        for (var i = 0; i < polygonArray.length; i++) {
            var item = polygonArray[i];
            item.setEditable(true);
        }
    }
}

function hidePolyline() {
    if (polylineArray && polylineArray.length > 0) {
        for (var i = 0; i < polylineArray.length; i++) {
            var item = polylineArray[i];
            item.setMap(null);
        }
    }
}

function showPolyline() {
    if (polylineArray && polylineArray.length > 0) {
        for (var i = 0; i < polylineArray.length; i++) {
            var item = polylineArray[i];
            item.setMap(map);
        }
    }
}

function editPolyline() {
    if (polylineArray && polylineArray.length > 0) {
        for (var i = 0; i < polylineArray.length; i++) {
            var item = polylineArray[i];
            item.setEditable(true);
        }
    }
}

function deleteMarker(id) {
    var itemMarker = markerArray[id - 1];
    itemMarker.setMap(null);
    markerArray.splice(id - 1, 1)
}

function deletePolyline(id) {
    var itemMarker = polylineArray[id - 1];
    itemMarker.setMap(null);
    polylineArray.splice(id - 1, 1)
}

function deletePolygon(id) {
    var itemMarker = polygonArray[id - 1];
    itemMarker.setMap(null);
    polygonArray.splice(id - 1, 1)
}

function addCircle() {
    endPolyline();
    endMarker();
    endPolygon();

    var circle;
    var addCircle1 = google.maps.event.addListener(map, "click", function(event) {
        var radius = 0;
        var markerCenter = new google.maps.Marker({ position: event.latLng, map: map, draggable: false, icon: image });
        var markerRadius = new google.maps.Marker({ position: event.latLng, map: map, draggable: true, title: 'title', icon: image });
        google.maps.event.addListener(markerRadius, 'drag', function(MouseEvent) {
            radius = calculationDistance(markerCenter.getPosition().lat(), markerCenter.getPosition().lng(), MouseEvent.latLng.lat(), MouseEvent.latLng.lng());
            circle.setRadius(radius * 1000);
        });
        var circleOptions = {
            map: map,
            center: event.latLng,
            radius: radius,
            strokeColor: "#FF0000",
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: "#FF0000",
            fillOpacity: 0.35
        };
        circle = new google.maps.Circle(circleOptions);
        circle.setMap(map);
    });
}

function endCircle() {
    if (addCircleArray && addCircleArray.length > 0) {
        for (var i = 0; i < addCircleArray.length; i++) {
            var item = addCircleArray[i];
            google.maps.event.removeListener(item);
            var itemMarker1 = markerCenterArray[i];
            itemMarker1.setMap(null);
            var itemMarker2 = markerRadiusArray[i];
            itemMarker2.setMap(null);
        }
    }
    if (circleArray && circleArray.length > 0) {
        for (var i = 0; i < circleArray.length; i++) {
            var item = circleArray[i];
            item.setEditable(false);
        }
    }
    markerCenterArray = new Array();
    markerRadiusArray = new Array();
    addCircleArray = new Array();
}

function hideCircle() {
    if (circleArray && circleArray.length > 0) {
        for (var i = 0; i < circleArray.length; i++) {
            var item = circleArray[i];
            item.setMap(null);
        }
    }
}

function showCircle() {
    if (circleArray && circleArray.length > 0) {
        for (var i = 0; i < circleArray.length; i++) {
            var item = circleArray[i];
            item.setMap(map);
        }
    }
}

function editCircle() {
    if (circleArray && circleArray.length > 0) {
        for (var i = 0; i < circleArray.length; i++) {
            var item = circleArray[i];
            item.setEditable(true);
        }
    }
}

function deleteCircle(id) {
    var itemMarker = circleArray[id - 1];
    itemMarker.setMap(null);
    circleArray.splice(id - 1, 1)
}

function devicePosition() {
    requestDevicePosition();
    setInterval(function() {
        requestDevicePosition();
    }, 20000);
}

function requestDevicePosition() {
    $.get("http://39.96.33.244:8080/pad/requestDevicePosition", [], function(result) {

        removePostMarkers();
        var total = 0;

        if (result && result.status == 0) {
            total = result.data.length;
            for (var i = 0; i < result.data.length; i++) {
                var item = result.data[i];
                var myLatLng = new google.maps.LatLng(parseFloat(item["lat"]), parseFloat(item["lng"]));
                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    title: item["phone"],
                    icon: image
                });

                postMarkerArray.push(marker);
            }
        }
    });
}

function removePostMarkers() {
    if (postMarkerArray && postMarkerArray.length > 0) {
        for (var i = 0; i < postMarkerArray.length; i++) {
            var itemMarker = postMarkerArray[i];
            itemMarker.setMap(null);

        }
    }
    postMarkerArray = new Array();
}